package userall;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
//最终版

public class User {
	// 昵称
	private String nickname;
	// 账号
	private String name;
	// 余额
	private int balance;
	// 电话号码
	private String phone;
	// 密码
	private String password;

	// setter和getter方法
	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public User(String nickname, String name, String phone, String password, int balance) {
		this.nickname = nickname;
		this.name = name;
		this.phone = phone;
		this.password = password;
		this.balance = balance;
	}

	/**
	 * 用户注册
	 * 
	 * @return 注册成功返回true
	 */
	public boolean register() {
		boolean flag;
		flag = this.checkFile();
		if (flag) {
			String filename = "txt\\user.txt";
			try (BufferedWriter bw = new BufferedWriter(new FileWriter(filename, true))) {
				bw.write(this.nickname);
				bw.write(" ");
				bw.write(this.name);
				bw.write(" ");
				bw.write(this.password);
				bw.write(" ");
				bw.write(Integer.toString(this.balance));
				bw.write(" ");
				bw.write(this.phone);
				bw.newLine();
				bw.flush();
				return true;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	/**
	 * 判断账号是否注册过
	 * 
	 * @return 未注册过返回true
	 */
	public boolean checkFile() {
		File f = new File("txt\\user.txt");
		try (BufferedReader br = new BufferedReader(new FileReader(f))) {
			String in;
			if ((in = br.readLine()) == null) {
				return true;
			} else {
				String[] input = in.split(" ");
				if (input[1].equals(name)) {
					return false;
				} else {
					while ((in = br.readLine()) != null) {
						input = in.split(" ");
						if (input[1].equals(name)) {
							return false;
						}
					}
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("未找到login.txt文件");
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		return true;
	}

	/**
	 * 用户登录
	 * 
	 * @param name
	 *            输入的账号
	 * @param password
	 *            输入的密码
	 * @return 登录成功返回true
	 */
	public boolean login(String name, String password) {
		File f = new File("txt\\user.txt");
		int flag = 0;
		try (BufferedReader br = new BufferedReader(new FileReader(f))) {
			String in;
			if ((in = br.readLine()) == null) {
				return false;
			} else {
				String[] input0 = in.split(" ");
				// 文件不为空，判断读入的第一行是否匹配
				if (input0[1].equals(name)) {
					if (input0[2].equals(password)) {
						this.name = name;
						this.password = password;
						this.nickname = input0[0];
						this.balance = Integer.parseInt(input0[3]);
						this.phone = input0[4];
						flag++;
					}
				}
				// 判断之后是否有更新后的内容
				while ((in = br.readLine()) != null) {
					String[] input = in.split(" ");
					if (input[1].equals(name)) {
						if (input[2].equals(password)) {
							this.name = input[1];
							this.password = input[2];
							this.nickname = input[0];
							this.balance = Integer.parseInt(input[3]);
							this.phone = input[4];
							flag++;
						}
					}
				}
				if (flag > 0) {
					return true;
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println("未找到login.txt文件");
		} catch (IOException e2) {
			e2.printStackTrace();
		}

		return false;
	}

	/**
	 * 为用户充值金额
	 * 
	 * @param money
	 *            必须大于0
	 * @return 充值成功返回true
	 */
	public boolean recharge(int money) {
		if (money <= 0) {
			return false;
		} else {
			this.balance += money;
			return true;
		}
	}

	/**
	 * 购买彩票：余额减去彩票价格乘以投注数
	 * 
	 * @param ticket
	 *            要购买的彩票
	 * @return 购买成功返回true
	 */
	public Boolean buyLottery(Tickets ticket) {
		if (this.balance >= ticket.getPrice() * ticket.getAmount()) {
			this.balance -= ticket.getPrice() * ticket.getAmount();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 中奖通知
	 * 
	 * @return 中奖返回奖金数，未中奖返回no
	 */
	public String inform() {
		String filename = "txt\\Record.txt";

		try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
			String in;

			if ((in = br.readLine()) == null) {
				return "no";
			} else {
				String[] input = in.split(" ");
				if (input[5].equals(this.name)) {
					if (!input[4].equals("0")) {
						return input[4];
					} else {
						return "no";
					}
				} else {
					while ((in = br.readLine()) != null) {
						input = in.split(" ");
						if (input[5].equals(this.name)) {
							if (!input[4].equals("0")) {
								return input[4];
							}
						}
					}
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "no";
	}

}
