package Manager;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.LineNumberReader;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import userall.Tickets;
import userall.UAndT;
import userall.User;

public class Manager {

	public Manager() {
		managerGUI();
	}

	public static void main(String[] args) {
		new Manager();
	}

	// 历史开奖
	private Vector<Vector<String>> createTableModelData_historyAward() {

		Vector<Vector<String>> data = new Vector<Vector<String>>();
		String t = null;
		try {
			FileReader fr1 = null;
			try {
				String fileName = "txt/History.txt";
				fr1 = new FileReader(fileName);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			BufferedReader br = new BufferedReader(fr1);
			while ((t = br.readLine()) != null) {
				String[] s = t.split("\\s+"); // 通过空格分割字符串数组
				Vector<String> rowData = new Vector<String>();

				// rowData.add("i");
				String temp = s[0] + " " + s[1];
				// rowData.add(s[0]);
				rowData.add(temp);
				rowData.add(s[2]);
				rowData.add(s[3]);
				rowData.add(s[4]);
				rowData.add(s[5]);
				data.add(rowData);
			}
			fr1.close();
			br.close();

		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();

		}
		return data;
	}

	private Vector<String> createColumnNames_historyAward() {
		Vector<String> columnNames = new Vector<String>();
		// columnNames.add("序号");
		columnNames.add("日期");
		columnNames.add("号码");
		columnNames.add("奖池");
		columnNames.add("用户名");
		columnNames.add("中奖金额");
		return columnNames;
	}

	public DefaultTableModel historyAward() {
		DefaultTableModel model = null;
		// 表头（列名）
		Vector<String> columnNames = createColumnNames_historyAward();
		Vector<Vector<String>> data = createTableModelData_historyAward();
		// 创建一个默认的表格模型
		model = new DefaultTableModel(data, columnNames);
		return model;
	}

	// 查看订单
	private Vector<Vector<String>> createTableModelData_showOrder() {

		Vector<Vector<String>> data = new Vector<Vector<String>>();
		String t = null;
		try {
			FileReader fr1 = null;
			try {
				String fileName = "txt/UAndT.txt";
				fr1 = new FileReader(fileName);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			BufferedReader br = new BufferedReader(fr1);
			while ((t = br.readLine()) != null) {
				String[] s = t.split("\\s+"); // 通过空格分割字符串数组
				Vector<String> rowData = new Vector<String>();

				// rowData.add("i");
				String temp = s[0] + " " + s[1];
				// rowData.add(s[0]);
				rowData.add(temp);
				rowData.add(s[2]);
				rowData.add(s[3]);
				rowData.add(s[4]);
				data.add(rowData);
			}
			fr1.close();
			br.close();

		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();

		}
		return data;
	}

	private Vector<String> createColumnNames_showOrder() {
		Vector<String> columnNames1 = new Vector<String>();
		// columnNames.add("序号");
		columnNames1.add("日期");
		columnNames1.add("账号");
		columnNames1.add("号码");
		columnNames1.add("注数");
		return columnNames1;
	}

	public DefaultTableModel showOrder() {
		DefaultTableModel model = null;
		// 表头（列名）
		Vector<String> columnNames1 = createColumnNames_showOrder();
		Vector<Vector<String>> data = createTableModelData_showOrder();
		// 创建一个默认的表格模型
		model = new DefaultTableModel(data, columnNames1);
		return model;
	}

	// 用户信息
	private Vector<Vector<String>> createTableModelData_showUsers() {

		Vector<Vector<String>> data = new Vector<Vector<String>>();
		String t = null;
		try {
			FileReader fr1 = null;
			try {
				String fileName = "txt/user.txt";
				fr1 = new FileReader(fileName);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			BufferedReader br = new BufferedReader(fr1);
			while ((t = br.readLine()) != null) {
				String[] s = t.split("\\s+"); // 通过空格分割字符串数组
				Vector<String> rowData = new Vector<String>();
				// String temp = s[0] + " "+s[1];
				// rowData.add(temp);
				rowData.add(s[0]);
				rowData.add(s[1]);
				rowData.add(s[2]);
				rowData.add(s[3]);
				rowData.add(s[4]);
				data.add(rowData);
			}
			fr1.close();
			br.close();

		} catch (IOException e) {
			// TODO 自动生成的 catch 块
			e.printStackTrace();

		}
		return data;
	}

	private Vector<String> createColumnNames_showUsers() {
		Vector<String> columnNames1 = new Vector<String>();
		// columnNames.add("序号");
		columnNames1.add("ID");
		columnNames1.add("账号");
		columnNames1.add("密码");
		columnNames1.add("余额");
		columnNames1.add("电话");
		return columnNames1;
	}

	public DefaultTableModel showUsers() {
		DefaultTableModel model = null;
		// 表头（列名）
		Vector<String> columnNames = createColumnNames_showUsers();
		Vector<Vector<String>> data = createTableModelData_showUsers();
		// 创建一个默认的表格模型
		model = new DefaultTableModel(data, columnNames);
		return model;
	}

	public int countLines(String filename) throws IOException {
		LineNumberReader reader = new LineNumberReader(new FileReader(filename));
		int cnt = 0;
		String lineRead = "";
		while ((lineRead = reader.readLine()) != null) {
		}

		cnt = reader.getLineNumber();
		reader.close();
		return cnt;
	}
	
	public int getMoney(String temp, Vector<String> vector1) {
		// 获得特殊数字
		String[] arr = temp.split(","); // 用,分割
		Arrays.toString(arr);
		int specialNumber = 0;
		int m;
		for (m = 0; m < arr.length; m++)
			specialNumber += Integer.parseInt(arr[m]);
		specialNumber = specialNumber % 30 + 1;
		// String number = UserGui.getUtlist().get(i).getTicket().getNumber();
		String number = vector1.get(3).toString();
		String[] arr1 = number.split(",");
		Arrays.toString(arr1);
		int count = 0;// 计算中奖号码数
		int flag = 0;
		int j,k;
		for ( j = 0; j < arr.length; j++) {
			for ( k = 0; k < arr1.length; k++) {
				if (Integer.parseInt(arr[j]) > Integer.parseInt(arr1[k]))
					k++;
				else if (Integer.parseInt(arr[j]) < Integer.parseInt(arr1[k]))
					j++;
				else
					count++;
				/*if (Integer.parseInt(arr1[k]) == specialNumber)数组越界问题
					flag = 1;*/
			}
			if(k==arr1.length)break;
		}
		for(int p=0;p<arr.length;p++) {
			if(Integer.parseInt(arr[p])==specialNumber)
			{
				flag=1;
				break;
			}
		}
		int award = 0;
		if (count == 7)
			award = 2500000;
		else if (count == 6 && flag == 1)
			award = 1000000;
		else if (count == 6 && flag == 0)
			award = 500000;
		else if (count == 5 && flag == 1)
			award = 200 * Integer.parseInt(vector1.get(4));
		else if (count == 5 && flag == 0)
			award = 50 * Integer.parseInt(vector1.get(4));
		else if (count == 4 && flag == 1)
			award = 10 * Integer.parseInt(vector1.get(4));
		else if (count == 4 && flag == 0)
			award = 5 * Integer.parseInt(vector1.get(4));
		return award;
	}

	private void modifyFile(JTextField jf)  {
		// TODO Auto-generated method stub

		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
		// dateFormat.format(date)"日期","号码", "奖池","用户名","中奖金额"
		String temp = jf.getText().toString();// 获得文本框的内容

		String filename = "txt\\History.txt";
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(filename, true))) {
			Vector<Vector<String>> vector = createTableModelData_showOrder();// 读取UAndT.txt内容存在vector里面
			//System.out.println(vector.size());
			for (int i = 0; i < vector.size(); i++) {
				int award = getMoney(temp, vector.get(i));
				// if(award!=0)
				// {
				//System.out.println(i);
				int r1 = countLines(filename);
				bw.write(dateFormat.format(date));
				bw.write(" ");
				// bw.write(UserGui.getUtlist().get(r1).getTicket().getNumber());
				bw.write((vector.get(i)).get(2));//号码
				bw.write(" ");
				bw.write(temp);
				bw.write(" ");
				// bw.write(UserGui.getUtlist().get(r1).getUser().getName());
				bw.write((vector.get(i)).get(1));//用户名
				bw.write(" ");
				bw.write(String.valueOf(award));
				bw.newLine();
				bw.flush();
			}
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		String filename1 = "txt\\Record.txt";
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(filename1))) {
			Vector<Vector<String>> vector = createTableModelData_showOrder();// 读取UAndT.txt内容存在vector里面
			Vector<Vector<String>> vector1 = createTableModelData_showUsers();// 读取user.txt内容存在vector里面
			for (int i = 0; i < vector.size(); i++) {

				//int r1 = countLines(filename);
				bw.write(dateFormat.format(date));
				bw.write(" ");
				bw.write((vector.get(i)).get(2));//号码
				bw.write(" ");
				bw.write((vector.get(i)).get(3));//注数
				bw.write(" ");
				bw.write((vector1.get(i)).get(3));//金额
				bw.write(" ");
				bw.write((vector.get(i)).get(1));//姓名
				bw.newLine();
				bw.flush();
			}
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static final String allChar = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
	public static final String numberChar = "0123456789";

	public static String generateString() // 参数为返回随机数的长度
	{
		StringBuffer sb = new StringBuffer();
		Random random = new Random();
		for (int i = 0; i < 8; i++) {
			sb.append(allChar.charAt(random.nextInt(allChar.length())));
		}
		return sb.toString();
	}

	public static String generatePhoneString() // 参数为返回随机数的长度
	{
		StringBuffer sb = new StringBuffer();
		Random random = new Random();
		sb.append('1');
		for (int i = 0; i < 10; i++) {
			sb.append(allChar.charAt(random.nextInt(numberChar.length())));
		}
		return sb.toString();
	}

	public static String generateNumber() {// 随机号码
		Random random = new Random();
		int[] r = new int[7];
		for (int i = 0; i < r.length;) {
			int temp = random.nextInt(36);
			if (temp == 0)
				continue;
			for (int j : r) {
				if (j == temp)
					continue;
			}
			r[i] = temp;
			i++;
		}
		StringBuilder str = new StringBuilder();
		for (int i = 0; i < r.length; i++) {
			if (i == 0)
				str.append(r[i]);
			else {
				str.append(",");
				str.append(r[i]);
			}
		}
		return str.toString();
	}

	public void Auto_Register(UAndT uat) throws IOException {
		String nikename = generateString();
		String name = generateString();
		String password = generateString();
		Random random = new Random();
		int balance = random.nextInt(100);
		String phone = generatePhoneString();
		User newUsers = new User(nikename, name, phone, password, balance); // 注册账户
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String pool = generateNumber();// 随机号码
		int multiple = random.nextInt(1000);
		Tickets ticket = new Tickets();
		ticket.setNumber(pool);
		ticket.setAmount(multiple);
		uat.buyTickets(newUsers, ticket);
		
		String filename = "txt\\UAndT.txt";
		//int r = countLines(filename);
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(filename, true))) {
			bw.write(dateFormat.format(date));
			bw.write(" ");
			bw.write(newUsers.getName());
			bw.write(" ");
			bw.write(pool);
			//bw.write(UserGui.getUtlist().get(r).getTicket().getNumber());
			bw.write(" ");
			bw.write(Integer.toString(multiple));
			bw.write(" ");
			bw.newLine();
			bw.flush();
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		newUsers.register();
		/*String filename1 = "txt\\user.txt";
		//int r1 = countLines(filename1);
		try (BufferedWriter bw = new BufferedWriter(new FileWriter(filename1, true))) {
			bw.write(newUsers.getNickname());
			bw.write(" ");
			bw.write(newUsers.getName());
			bw.write(" ");
			bw.write(newUsers.getPassword());
			//bw.write(UserGui.getUtlist().get(r).getTicket().getNumber());
			bw.write(" ");
			bw.write(String.valueOf(newUsers.getBalance()));
			bw.write(" ");
			bw.write(newUsers.getPhone());
			bw.write(" ");
			bw.newLine();
			bw.flush();
			bw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}*/

	}

	public void AutoRegister(String string) {
		UAndT uat = new UAndT();
		int num = Integer.parseInt(string);
		Thread t1 = new Thread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				for (int i = 0; i < num; i++) {
					try {
						Auto_Register(uat);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					// UserGui.getUtlist().add(uat);
				}
			}
		});
		t1.start();
	}

	public void location(JFrame menu) {
		// int windowWidth = menu.getWidth(); // 获取窗体的宽
		int windowHeight = menu.getHeight();// 获取窗体的高
		Toolkit kit = Toolkit.getDefaultToolkit(); // 定义工具包
		Dimension screenSize = kit.getScreenSize(); // 获取屏幕的尺寸
		int screenWidth = screenSize.width; // 获取屏幕的宽
		int screenHeight = screenSize.height; // 获取屏幕的高
		menu.setLocation(screenWidth / 2 - 300, screenHeight / 2 - windowHeight / 2);
	}

	public void managerGUI() {
		JFrame menu = new JFrame();
		menu.setLayout(null); // 清除布局函数
		menu.setSize(300, 400);
		menu.setResizable(false); // 设置窗体大小不可变
		menu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		menu.setLayout(new BorderLayout()); // 新建BorderLayout布局

		JPanel panel1 = new JPanel(new FlowLayout());
		JLabel jl1 = new JLabel("欢迎使用管理员");
		
		//插入图片
		/*ImageIcon image = new ImageIcon("image\\2.jpg");
        JLabel imagelabel = new JLabel(image);
        imagelabel.setBounds(10,10,100,100);
        panel1.add(imagelabel);*/
		/*JPanel bj = new JPanel() {// 设置背景
			@Override
			protected void paintComponent(Graphics g) {
				Image bg;
				try {
					bg = ImageIO.read(new File("image/2.jpg"));
					g.drawImage(bg, 0, 0, getWidth(), getHeight(), null);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		};
		//bj.setBorder(new EmptyBorder(10, 10, 0, 10));
		menu.add(bj,BorderLayout.NORTH);*/
        
		jl1.setFont(new Font("微软雅黑", Font.BOLD, 30)); //设置字体样式和大小
		panel1.add(jl1);
		panel1.setBorder(new EmptyBorder(10, 10, 0, 10));
		menu.add(panel1, BorderLayout.NORTH);

		JPanel panel2 = new JPanel(new GridLayout(5, 1));

		JButton jb1 = new JButton("开奖");
		jb1.addActionListener(new java.awt.event.ActionListener() {
			// @SuppressWarnings("deprecation")
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				JFrame menu1 = new JFrame("开奖");
				menu1.setLayout(new BorderLayout());
				JPanel jp1 = new JPanel();
				JPanel jp2 = new JPanel();
				JTextField jf = new JTextField(15);
				jf.setFont(new Font("微软雅黑", Font.BOLD, 18));
				JButton jb1 = new JButton("开始");
				MyThread mt = new MyThread(jf);
				jb1.addActionListener((ActionEvent e) -> {
					jf.setText("");
					mt.start();
				});
				JButton jb2 = new JButton("停止");
				jb2.addActionListener((ActionEvent e) -> {
					mt.stopping();
					modifyFile(jf);
					/*try {
						modifyFile(jf);
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}*/
				});
				/*
				 * 特码为七个数字之和对36取余加1
				 */
				jp1.add(jf);
				jp2.add(jb1);
				jp2.add(jb2);
				menu1.add(jp1, BorderLayout.NORTH);
				menu1.add(jp2, BorderLayout.CENTER);
				menu1.pack();
				location(menu1);
				menu1.setVisible(true);
			}
		});

		JButton jb2 = new JButton("历史开奖");
		jb2.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {

				JFrame menu2 = new JFrame("历史开奖");
				JPanel jpanel1 = new JPanel(new FlowLayout());
				JScrollPane scrollPane1 = new JScrollPane();
				scrollPane1.setBounds(80, 25, 700, 500);
				jpanel1.add(scrollPane1);
				JTable table1 = new JTable();
				DefaultTableModel model = historyAward();
				table1 = new JTable(model);
				table1.setBounds(100, 100, 200, 200);
				scrollPane1.setPreferredSize(new Dimension(700, 500));
				scrollPane1.setAutoscrolls(true);
				scrollPane1.setViewportView(table1);
				menu2.add(jpanel1);
				menu2.pack();
				location(menu2);
				menu2.setVisible(true);
			}
		});

		JButton jb3 = new JButton("订单");
		jb3.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {

				JFrame menu3 = new JFrame("订单");
				JPanel jpanel2 = new JPanel(new FlowLayout());
				JScrollPane scrollPane2 = new JScrollPane();
				scrollPane2.setBounds(80, 25, 700, 500);
				jpanel2.add(scrollPane2);
				JTable table2 = new JTable();
				DefaultTableModel model1 = showOrder();
				table2 = new JTable(model1);
				table2.setBounds(100, 100, 200, 200);

				scrollPane2.setPreferredSize(new Dimension(700, 500));// 设置表格大小
				scrollPane2.setAutoscrolls(true);
				scrollPane2.setViewportView(table2);
				menu3.add(jpanel2);
				menu3.pack();
				location(menu3);
				menu3.setVisible(true);
			}
		});

		JButton jb4 = new JButton("用户信息");
		jb4.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				JFrame menu2 = new JFrame("客户信息");
				JPanel jpanel1 = new JPanel(new FlowLayout());
				JScrollPane scrollPane1 = new JScrollPane();
				scrollPane1.setBounds(80, 25, 700, 500);
				jpanel1.add(scrollPane1);
				JTable table1 = new JTable();
				DefaultTableModel model = showUsers();
				table1 = new JTable(model);
				table1.setBounds(100, 100, 200, 200);
				scrollPane1.setPreferredSize(new Dimension(700, 500));
				scrollPane1.setAutoscrolls(true);
				scrollPane1.setViewportView(table1);
				menu2.add(jpanel1);
				menu2.pack();
				location(menu2);
				menu2.setVisible(true);
			}
		});

		JButton jb5 = new JButton("测试");
		jb5.addActionListener(new java.awt.event.ActionListener() {
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				JFrame menu = new JFrame("自动注册买票");
				menu.setLayout(new BorderLayout());
				JPanel jp1 = new JPanel();
				JPanel jp2 = new JPanel();
				JTextField jf = new JTextField(10);
				jf.setFont(new Font("微软雅黑", Font.BOLD, 18));
				JButton jb1 = new JButton("开始");

				jb1.addActionListener((ActionEvent e) -> {
					AutoRegister(jf.getText());
					JOptionPane.showMessageDialog(null, "用户注册购票成功", null, JOptionPane.OK_OPTION);
				});

				jp1.add(jf);
				jp2.add(jb1);
				JLabel jl = new JLabel("请输入注册用户数");
				menu.add(jl, BorderLayout.WEST);
				menu.add(jp1, BorderLayout.CENTER);
				menu.add(jp2, BorderLayout.SOUTH);
				menu.pack();
				location(menu);
				menu.setVisible(true);
			}
		});

		int w = 330;
		int h = 40;
		jb1.setPreferredSize(new Dimension(w, h));
		jb2.setPreferredSize(new Dimension(w, h));
		jb3.setPreferredSize(new Dimension(w, h));
		jb1.setFont(new java.awt.Font("黑体", 1, 20));
		jb2.setFont(new java.awt.Font("黑体", 1, 20));
		jb3.setFont(new java.awt.Font("黑体", 1, 20));
		jb4.setFont(new java.awt.Font("黑体", 1, 20));
		jb5.setFont(new java.awt.Font("黑体", 1, 20));
		panel2.add(jb1);
		panel2.add(jb2);
		panel2.add(jb3);
		panel2.add(jb4);
		panel2.add(jb5);
		panel2.setBorder(new EmptyBorder(20, 20, 20, 20));
		menu.add(panel2);
		menu.pack();
		location(menu);
		menu.setVisible(true);

	}
}
