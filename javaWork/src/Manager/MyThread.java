package Manager;

import java.util.Arrays;

import javax.swing.JTextField;

public class MyThread extends Thread{//使用Runnable实现多线程可以达到资源共享目的
	JTextField jf;
	public volatile Boolean isPause=true;//暂停标志
	public MyThread(JTextField jf) {
		super();
		this.jf = jf;
	}
	@Override
	public void run() {		 
		while(true){
			try {
				MyThread.sleep(100);
			}catch(InterruptedException e) {
				e.printStackTrace();
			}
			while(isPause) {
				int[] ball = new int[7];
				for(int i = 0;i<7;i++) {
					ball[i] = (int) (1+(Math.random()*36));
					for(int j=0;j<i;j++) {
						if(ball[i]==ball[j]) i--;//随机产生不重复的七个数字
					}
				}
				Arrays.sort(ball);			
				jf.setText(ball[0]+","+ball[1]+","+ball[2]+","+ball[3]+","+ball[4]+","+ball[5]+","+ball[6]);
			}
			
		}		
	}
	public void stopping() {
		isPause=false;
	}
}
