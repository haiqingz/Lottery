package javaWork;

public abstract class User {
	//用户ID，用户名，账户金额，电话号码 *实名认证 *支付密码
	private int id;              //用户ID
	private String name;         //用户名
	private int money;           //用户金额
	private String phone;        //电话号码
	private String truename;     //实名
	private String idnum;        //身份证号
	private String password;     //登录密码
	private String buypassword;  //支付密码
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getTruename() {
		return truename;
	}
	public void setTruename(String truename) {
		this.truename = truename;
	}
	public String getIdnum() {
		return idnum;
	}
	public void setIdnum(String idnum) {
		this.idnum = idnum;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getBuypassword() {
		return buypassword;
	}
	public void setBuypassword(String buypassword) {
		this.buypassword = buypassword;
	}

    public void login(String name,String password) {
    	if(name.equals(password)) {
    		
    	}
    	
    }
    public void showInfor() {
    	println();
    	
    }
    public void recharge() {
    	
    }
    public void buyLottery() {
    	
    }
    
    public abstract void println();
    

}
