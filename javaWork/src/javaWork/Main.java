package javaWork;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.peer.RobotPeer;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.Border;

public class Main {
	    /**
	     * 生成登录界面   
	     */
	    private static void createGUIlogin() {
	        //Create and set up the window.
	        JFrame frame = new JFrame("七乐彩彩票");
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    
	        //第一个窗口的组件
	        ImageIcon iconbj = new ImageIcon("image\\bj001.jpg");
	        ImageIcon iconname = new ImageIcon("image\\name.png");
	        ImageIcon iconpass = new ImageIcon("image\\suo.png");
	        JButton userbutt = new JButton("客户");
	        JButton regibutt = new JButton("注册");
	        JLabel label1 = new JLabel("账户", iconname, SwingConstants.CENTER);
	        JLabel label2 = new JLabel("密码",iconpass,SwingConstants.CENTER);
	        JLabel label3 = new JLabel(iconbj);
	        JTextField nametext = new JTextField();
	        JTextField passtext = new JTextField();
	        JPanel p1 = new JPanel();
	        JPanel p2 = new JPanel(new GridLayout(2,2));
	        JPanel p3 = new JPanel();
	        
	        JPanel p21 = new JPanel(new GridLayout(3, 0));
	        JPanel p22 = new JPanel(new GridLayout(3, 0));
	        JPanel p211 = new JPanel();
	        JPanel p221 = new JPanel();
	        
	        
	        //设置组件属性
	        label1.setFont(new Font("", 1, 20));
	        label1.setForeground(Color.BLACK);
	        label2.setFont(new Font("",1,20));
	        label2.setForeground(Color.BLACK);

	        //窗口显示位置
	        Dimension displaySize = Toolkit.getDefaultToolkit().getScreenSize();
	        Dimension frameSize = frame.getSize();
	        if(frameSize.width > displaySize.width)
	        frameSize.width = displaySize.width;
	        if(frameSize.height > displaySize.height) 
	        frameSize.height = displaySize.height;
	        frame.setLocation((displaySize.width - frameSize.width)/3,
	        (displaySize.height - frameSize.height)/3);
	        
	        //构建登录界面的窗体
	        p1.add(label3);
	        p2.add(label1);   
	        p2.add(p21);
	        p2.add(label2);
	        p2.add(p22);
	        p21.add(p211);
	        p21.add(nametext);
	        p22.add(p221);
	        p22.add(passtext);
	        p3.add(userbutt);
	        p3.add(regibutt);
	        
	        //设置窗体
	        frame.setLayout(new BorderLayout());
	        frame.add(p1,BorderLayout.NORTH);
	        frame.add(p2,BorderLayout.CENTER);
	        frame.add(p3,BorderLayout.SOUTH);
	        frame.setResizable(false);
	        frame.pack();
	        frame.setVisible(true);
	    }
	    /**
	     * 生成注册界面
	     */
	    public static void regisGUI() {
	    	JFrame frame = new JFrame("注册");
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        
	        //注册窗口的的组件
	        ImageIcon iconbj = new ImageIcon("image\\zcbj.png");
	        ImageIcon iconname = new ImageIcon("image\\zhuce.png");
	        ImageIcon iconpass = new ImageIcon("image\\zcpa.png");
	        ImageIcon iconphone = new ImageIcon("image\\phone.jpg");
	        
	        JPanel p1 = new JPanel(new GridLayout(4,2));
	        JPanel p2 = new JPanel();
	        JPanel p3 = new JPanel();
	        JLabel jl1 = new JLabel(iconbj);
	        	        
	        JLabel namejl = new JLabel("用 户 名",iconname,SwingConstants.CENTER);
 	        JLabel passjl = new JLabel("新  密 码",iconpass,SwingConstants.CENTER);
 	        JLabel cpassjl = new JLabel("确认密码",iconpass,SwingConstants.CENTER); 
 	        JLabel phonejl = new JLabel("电       话",iconphone,SwingConstants.CENTER);
 	        JTextField nametf = new JTextField();
 	        JTextField passtf = new JTextField();
 	        JTextField cpasstf = new JTextField();
 	        JTextField phonetf = new JTextField();
 	        JButton nowregibu = new JButton("立即注册");
 	        JButton gobackbu = new JButton("返回");
 	        
 	        JPanel p11 = new JPanel(new GridLayout(3,0));
	        JPanel p22 = new JPanel(new GridLayout(3,0));
	        JPanel p33 = new JPanel(new GridLayout(3,0));
	        JPanel p44 = new JPanel(new GridLayout(3,0));
 	        JPanel p111 = new JPanel();
	        JPanel p222 = new JPanel();
	        JPanel p333 = new JPanel();
	        JPanel p444 = new JPanel();
 	        
 	        //设置组件属性
 	       
	        namejl.setFont(new Font("宋体",1,20));
	        namejl.setForeground(Color.black);
	        passjl.setFont(new Font("",1,20));
	        cpassjl.setFont(new Font("",1,20));
	        phonejl.setFont(new Font("",1,20));
	        nowregibu.setFont(new Font("黑体",1,15));
	        nowregibu.setForeground(Color.RED);
	        gobackbu.setFont(new Font("黑体", 1, 15));
	        
 	        
	        
 	        //构建注册界面的窗体
	        p2.add(jl1);
	        
 	        p1.add(namejl);
 	        p1.add(p11);
 	        p1.add(passjl);
 	        p1.add(p22);
 	        p1.add(cpassjl);
 	        p1.add(p33);
 	        p1.add(phonejl);
 	        p1.add(p44);
 	        
 	       
 	        p3.add(nowregibu);
 	        p3.add(gobackbu);
 	       
 	        p11.add(p111);
 	        p11.add(nametf);
 	        p22.add(p222);
 	        p22.add(passtf);
 	        p33.add(p333);
 	        p33.add(cpasstf);
 	        p44.add(p444);
 	        p44.add(phonetf);
 	       
 	        
 	        //窗口显示位置
	        Dimension displaySize = Toolkit.getDefaultToolkit().getScreenSize();
	        Dimension frameSize = frame.getSize();
	        if(frameSize.width > displaySize.width)
	        frameSize.width = displaySize.width;
	        if(frameSize.height > displaySize.height) 
	        frameSize.height = displaySize.height;
	        frame.setLocation((displaySize.width - frameSize.width)/3,
	        (displaySize.height - frameSize.height)/3);
	        
	        //设置窗体
	        frame.setLayout(new BorderLayout());
	        
	        frame.add(p1,BorderLayout.CENTER);
	        frame.add(p2,BorderLayout.NORTH);
	        frame.add(p3,BorderLayout.SOUTH);

	        frame.setSize(400, 550);
	        frame.setResizable(false);      
	        frame.setVisible(true);
	    }
	    /**
	     * 生成客户端窗口
	     * @param args
	     */
	    public static void clientGUI() {
	    	JFrame frame = new JFrame("七彩乐彩票");
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        
	        //窗口的的组件
	        ImageIcon icon = new ImageIcon("image\\zhuce.png"); 
	        JPanel p1 = new JPanel(new GridLayout(1,3));
	        JPanel p2 = new JPanel(new GridLayout(3,10));
	        JPanel p3 = new JPanel(new GridLayout(3,1));
	        JPanel p11 = new JPanel();
	        JPanel p12 = new JPanel(new GridLayout(3,1));
	        JPanel p13 = new JPanel();
	        JPanel p121 = new JPanel(new GridLayout(1,3));
	        JPanel p1211 = new JPanel();
	        
	        JPanel p31 = new JPanel(new GridLayout(1,7));
	        JPanel p32 = new JPanel(new GridLayout(1,3));
	        JPanel p322 = new JPanel();
	        JPanel p33 = new JPanel();
	        
	        JLabel tzjl = new JLabel("投注数");
	        JTextField num1jt = new JTextField();
	        JTextField num2jt = new JTextField();
	        JTextField num3jt = new JTextField();
	        JTextField num4jt = new JTextField();
	        JTextField num5jt = new JTextField();
	        JTextField num6jt = new JTextField();
	        JTextField num7jt = new JTextField();
	        
	        JTextField touzhujt = new JTextField(6);
	        JButton pchosebu = new JButton("手动选号");
	        JButton mchosebu = new JButton("随机选号");
	        JButton buybu = new JButton("购买彩票");
	        
	        ImageIcon ic1 = new ImageIcon("imagebu\\01.png");
	        ImageIcon ic2 = new ImageIcon("imagebu\\02.png");
	        ImageIcon ic3 = new ImageIcon("imagebu\\03.png");
	        ImageIcon ic4 = new ImageIcon("imagebu\\04.png");
	        ImageIcon ic5 = new ImageIcon("imagebu\\05.png");
	        ImageIcon ic6 = new ImageIcon("imagebu\\06.png");
	        ImageIcon ic7 = new ImageIcon("imagebu\\07.png");
	        ImageIcon ic8 = new ImageIcon("imagebu\\08.png");
	        ImageIcon ic9 = new ImageIcon("imagebu\\09.png");
	        ImageIcon ic10 = new ImageIcon("imagebu\\10.png");
	        ImageIcon ic11 = new ImageIcon("imagebu\\11.png");
	        ImageIcon ic12 = new ImageIcon("imagebu\\12.png");
	        ImageIcon ic13 = new ImageIcon("imagebu\\13.png");
	        ImageIcon ic14 = new ImageIcon("imagebu\\14.png");
	        ImageIcon ic15 = new ImageIcon("imagebu\\15.png");
	        ImageIcon ic16 = new ImageIcon("imagebu\\16.png");
	        ImageIcon ic17 = new ImageIcon("imagebu\\17.png");
	        ImageIcon ic18 = new ImageIcon("imagebu\\18.png");
	        ImageIcon ic19 = new ImageIcon("imagebu\\19.png");
	        ImageIcon ic20 = new ImageIcon("imagebu\\20.png");
	        ImageIcon ic21 = new ImageIcon("imagebu\\21.png");
	        ImageIcon ic22 = new ImageIcon("imagebu\\22.png");
	        ImageIcon ic23 = new ImageIcon("imagebu\\23.png");
	        ImageIcon ic24 = new ImageIcon("imagebu\\24.png");
	        ImageIcon ic25 = new ImageIcon("imagebu\\25.png");
	        ImageIcon ic26 = new ImageIcon("imagebu\\26.png");
	        ImageIcon ic27 = new ImageIcon("imagebu\\27.png");
	        ImageIcon ic28 = new ImageIcon("imagebu\\28.png");
	        ImageIcon ic29 = new ImageIcon("imagebu\\29.png");
	        ImageIcon ic30 = new ImageIcon("imagebu\\30.png");
	        
	        JButton b1 = new JButton();
	        JButton b2 = new JButton();
	        JButton b3 = new JButton();
	        JButton b4 = new JButton();
	        JButton b5 = new JButton();
	        JButton b6 = new JButton();
	        JButton b7 = new JButton();
	        JButton b8 = new JButton();
	        JButton b9 = new JButton();
	        JButton b10 = new JButton();
	        JButton b11 = new JButton();
	        JButton b12 = new JButton();
	        JButton b13 = new JButton();
	        JButton b14 = new JButton();
	        JButton b15 = new JButton();
	        JButton b16 = new JButton();
	        JButton b17 = new JButton();
	        JButton b18 = new JButton();
	        JButton b19 = new JButton();
	        JButton b20 = new JButton();
	        JButton b21 = new JButton();
	        JButton b22 = new JButton();
	        JButton b23 = new JButton();
	        JButton b24 = new JButton();
	        JButton b25 = new JButton();
	        JButton b26 = new JButton();
	        JButton b27 = new JButton();
	        JButton b28 = new JButton();
	        JButton b29 = new JButton();
	        JButton b30 = new JButton();
	        
	        JButton perinforbu = new JButton();
	        JButton rechargebu = new JButton("充值");
	        JButton reloginbu = new JButton("注销");
	        
	        //组件的属性
	        perinforbu.setIcon(icon);
	        b1.setIcon(ic1);
	        b2.setIcon(ic2);
	        b3.setIcon(ic3);
	        b4.setIcon(ic4);
	        b5.setIcon(ic5);
	        b6.setIcon(ic6);
	        b7.setIcon(ic7);
	        b8.setIcon(ic8);
	        b9.setIcon(ic9);
	        b10.setIcon(ic10);
	        b11.setIcon(ic11);
	        b12.setIcon(ic12);
	        b13.setIcon(ic13);
	        b14.setIcon(ic14);
	        b15.setIcon(ic15);
	        b16.setIcon(ic16);
	        b17.setIcon(ic17);
	        b18.setIcon(ic18);
	        b19.setIcon(ic19);
	        b20.setIcon(ic20);
	        b21.setIcon(ic21);
	        b22.setIcon(ic22);
	        b23.setIcon(ic23);
	        b24.setIcon(ic24);
	        b25.setIcon(ic25);
	        b26.setIcon(ic26);
	        b27.setIcon(ic27);
	        b28.setIcon(ic28);
	        b29.setIcon(ic29);
	        b30.setIcon(ic30);
	        
	        
	        
	        
	        //构建注册界面的窗体
	        p2.add(b1);
	        p2.add(b2);
	        p2.add(b3);
	        p2.add(b4);
	        p2.add(b5);
	        p2.add(b6);
	        p2.add(b7);
	        p2.add(b8);
	        p2.add(b9);
	        p2.add(b10);
	        p2.add(b11);
	        p2.add(b12);
	        p2.add(b13);
	        p2.add(b14);
	        p2.add(b15);
	        p2.add(b16);
	        p2.add(b17);
	        p2.add(b18);
	        p2.add(b19);
	        p2.add(b20);
	        p2.add(b21);
	        p2.add(b22);
	        p2.add(b23);
	        p2.add(b24);
	        p2.add(b25);
	        p2.add(b26);
	        p2.add(b27);
	        p2.add(b28);
	        p2.add(b29);
	        p2.add(b30);
	        
	        p1.add(p13);
	        p1.add(p11);        
	        p1.add(p12);
	        p11.add(perinforbu);
	        p12.add(p121);
	        p121.add(p1211);
	        p121.add(rechargebu);
	        p121.add(reloginbu);
	        
	        
	        p3.add(p31);       
	        p3.add(p32);
	        p3.add(p33);
	        
	        p31.add(num1jt);
	        p31.add(num2jt);
	        p31.add(num3jt);
	        p31.add(num4jt);
	        p31.add(num5jt);
	        p31.add(num6jt);
	        p31.add(num7jt);
	        
	        
	        p32.add(pchosebu);
	        p32.add(p322);
	        p32.add(mchosebu);
	        p322.add(tzjl);
	        p322.add(touzhujt);
	        
	        p33.add(buybu);
	        
	        //窗口显示位置
	        Dimension displaySize = Toolkit.getDefaultToolkit().getScreenSize();
	        Dimension frameSize = frame.getSize();
	        if(frameSize.width > displaySize.width)
	        frameSize.width = displaySize.width;
	        if(frameSize.height > displaySize.height) 
	        frameSize.height = displaySize.height;
	        frame.setLocation((displaySize.width - frameSize.width)/3,
	        (displaySize.height - frameSize.height)/3);
	        
	        
	        //设置窗体
	        frame.setLayout(new BorderLayout());
	        frame.add(p1,BorderLayout.NORTH);
	        frame.add(p2,BorderLayout.CENTER);
	        frame.add(p3,BorderLayout.SOUTH);
	        frame.pack();
	        //frame.setSize(400, 550);
	        frame.setResizable(false);      
	        frame.setVisible(true);
	    }
	    
	    
	    public static void psnlMesgGUI() {
	    	JFrame frame = new JFrame("个人信息");
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        
	        ///图片
	        ImageIcon headicon = new ImageIcon("image\\headImag.jpg");
	        
	        ///组件
	        JPanel headJp = new JPanel(new GridLayout(2,1));
	        JPanel accJp = new JPanel(new GridLayout(6,3));
	        JPanel buttJp = new JPanel();
	        
	        JButton recharButt = new JButton("充值");
	        JButton drawMonButt = new JButton("提现");
	        
	        JLabel headjl = new JLabel("",headicon,SwingConstants.CENTER);
	        
	        JLabel accBal = new JLabel("账户余额");
	        JTextField bal = new JTextField();
	        
	        // 属性 更改
	        accBal.setFont(new Font("黑体",1,15));
	        bal.setFont(new Font("黑体",1,20));
	        recharButt.setFont(new Font("",1,20));
	        
	        drawMonButt.setFont(new Font("",1,20));
	        
	        headJp.add(headjl);
	        accJp.add(accBal);
	        accJp.add(bal);
	        buttJp.add(recharButt);
	        buttJp.add(drawMonButt);
	        
	        
	      //窗口显示位置
	        Dimension displaySize = Toolkit.getDefaultToolkit().getScreenSize();
	        Dimension frameSize = frame.getSize();
	        if(frameSize.width > displaySize.width)
	        frameSize.width = displaySize.width;
	        if(frameSize.height > displaySize.height) 
	        frameSize.height = displaySize.height;
	        frame.setLocation((displaySize.width - frameSize.width)/3,
	        (displaySize.height - frameSize.height)/3);
	        
	        //设置窗体
	        frame.setLayout(new BorderLayout());
	        
	        frame.add(headJp,BorderLayout.NORTH);
	        frame.add(accJp,BorderLayout.CENTER);
	        frame.add(buttJp,BorderLayout.SOUTH);

	        frame.setSize(400, 550);
	        frame.setResizable(false);      
	        frame.setVisible(true);
		}
	    
	    
	    public static void recharGUI() {
	    	JFrame frame = new JFrame("充值界面");
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        
//	        ImageIcon qrCode = new ImageIcon("image\\qrcode.png");
//	        ImageIcon qrCode1 = new ImageIcon("image\\qrcode1.jpg");
	        
	        JPanel monJp = new JPanel(new GridLayout(2,1));
	        JPanel monChoseJp = new JPanel(new GridLayout(4,2));
	        JLabel choseMon = new JLabel("请选择您需要充值的金额：");
	        JRadioButton monOne = new JRadioButton("100");
	        JRadioButton monTwo = new JRadioButton("500");
	        JRadioButton monthr = new JRadioButton("1000");
	        JRadioButton monFou = new JRadioButton("10000");
	        ButtonGroup choiceButt = new ButtonGroup();
	        choiceButt.add(monOne);
	        choiceButt.add(monTwo);
	        choiceButt.add(monthr);
	        choiceButt.add(monFou);
//	        String model = null;
	        
	        JPanel chosePsnlMonJp = new JPanel(new GridLayout(5,5));
	        JLabel chosePsnlMon = new JLabel("请输入您需要充值的金额：");
	        JTextField monText = new JTextField();
	        
//	        JPanel qrCodeJp = new JPanel(new GridLayout(1,2));
//	        JLabel qrCodeJl = new JLabel(qrCode);
//	        JLabel qrCodeJl1 = new JLabel(qrCode1);
	        
	        JPanel buttJp = new JPanel();
	        JButton recharButt = new JButton("支付");
	        JButton quitButt = new JButton("退出");
	        
	        monChoseJp.add(choseMon);
	        monChoseJp.add(monOne);
	        monChoseJp.add(monTwo);
	        monChoseJp.add(monthr);
	        monChoseJp.add(monFou);
	        chosePsnlMonJp.add(chosePsnlMon);
	        chosePsnlMonJp.add(monText);
//	        monJp.add(monChoseJp,BorderLayout.NORTH);
//	        monJp.add(chosePsnlMonJp,BorderLayout.SOUTH);
	        
//	        qrCodeJp.add(qrCodeJl);
//	        qrCodeJp.add(qrCodeJl1);
	        buttJp.add(recharButt);
	        buttJp.add(quitButt);
	        
	        
	        //窗口显示位置
	        Dimension displaySize = Toolkit.getDefaultToolkit().getScreenSize();
	        Dimension frameSize = frame.getSize();
	        if(frameSize.width > displaySize.width)
	        frameSize.width = displaySize.width;
	        if(frameSize.height > displaySize.height) 
	        frameSize.height = displaySize.height;
	        frame.setLocation((displaySize.width - frameSize.width)/3,
	        (displaySize.height - frameSize.height)/3);
	        
	        //设置窗体
	        frame.setLayout(new BorderLayout());
	        
	        frame.add(monChoseJp,BorderLayout.NORTH);
	        frame.add(chosePsnlMonJp,BorderLayout.CENTER);
	        frame.add(buttJp,BorderLayout.SOUTH);
	        

	        frame.setSize(400,400);
	        frame.setResizable(false);      
	        frame.setVisible(true);
	        
		}
	    
	    public static void payOver() {
	    	JOptionPane.showMessageDialog(null, "支付成功！！！");
	    	System.exit(0);
	    }

	    public interface RobotListener {
	    	
		}
	    
	    public class listenerRobot implements RobotListener{
	    	
	    }
	    
	    
	    
	    public static void main(String[] args) {
//	    	createGUIlogin();
//	    	regisGUI();
//    	    clientGUI();
//	    	psnlMesgGUI();
	    	recharGUI();
//	    	payOver();
	    	
	    	
	    	/* javax.swing.SwingUtilities.invokeLater(new Runnable() {
	            public void run() {
	                createAndShowGUI();
	            }
	        });*/
	        
	        
	    }
	    
}
    

    	
    
    	    

    	   

    	

  
	 


