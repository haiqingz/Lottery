package userall;

public class UAndT {
	// 用户账号
	private User user;
	// 彩票
	private Tickets ticket;

	// setter和gertter方法
	public Tickets getTicket() {
		return ticket;
	}

	public void setTicket(Tickets ticket) {
		this.ticket = ticket;
	}

	

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * 购买彩票
	 * 
	 * @param name
	 *            账号
	 * @param tickets
	 *            彩票
	 */
	public void buyTickets(User user, Tickets tickets) {
		this.user = user;
		this.ticket = tickets;
	}

}
