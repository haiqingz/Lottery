package userall;

public class Tickets {

	// 彩票号码
	private String number;
	// 单价
	private int price = 20;
	// 投注数
	private int amount = 0;

	// setter和getter方法
	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public int getPrice() {
		return price;
	}

}
